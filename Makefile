SHELL:=/bin/bash
PATH:=.:./orm/build/src/puzzlegen:$(PATH)

default:
	git submodule update --init --recursive
	make -C orm

unstrip:
	parallel --ungroup --bar -N1 'test -s {}.jl || { to-ormulogun-jl {} | unstrip-jl > {}.jl; }' :::: <(find cache -name '*.json')

.PHONY: default unstrip
