A collection of scripts to fetch and analyse the Lichess puzzle set.

Can optionally use Ormulogun's features for automatic puzzle tagging.

Released under the Apache License, version 2.

Dependencies
============

* PHP (CLI)
* parallel
* (see also Ormulogun and Gumble dependencies)

Typical usage
=============

~~~
./fetch-puzzles
make -C orm && make unstrip # Optional, tag puzzles using Ormulogun
./to-tsv > lichess.tsv
mkdir svg
parallel './{} > svg/{}.svg' ::: plot-*
~~~
